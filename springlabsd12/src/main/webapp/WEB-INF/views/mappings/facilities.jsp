<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>Bidirectional Mappings</h1>
<h3>Bidirectional One-to-Many</h3>
<hr/>
<h4>Facilities</h4>
<p>
Since we have a bidirectional mapping from Facilities to
Team Members, can query and list parent and child objects.
</p>
<c:forEach items="${facilityList}" var="facility">
	<div><strong>* ${facility}</strong></div>
	<c:forEach items="${facility.teammemberList}" var="teammember">	
		<div>** ${teammember}</div>
	</c:forEach>
</c:forEach>

<hr/>

<h4>Team Members</h4>
<c:forEach items="${teammemberList}" var="member">
	<div><strong>* ${member}</strong></div>
	<div>** ${member.facility}</div>
</c:forEach>

<hr/>

