<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>Standard Form Submit DTO/Entity</h1>

<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-12">
				<form method="post" id="form" action="<c:url value='standard-form-submit-dto' />">
					<div class="form-group col-sm-12">
						<div class="col-sm-6 form-group">
							<label for="firstName">First Name</label>
							<orly-input id="firstName" name="firstName" placeholder="First Name"></orly-input>
						</div>
						<div class="col-sm-6 form-group">
							<label for="lastName">Last Name</label>
							<orly-input id="lastName" name="lastName" placeholder="Last Name"></orly-input>
						</div>
						<div class="col-sm-3 form-group">
							<button id="submitBtn" type="submit" class="btn btn-primary">Submit</button>
							
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>