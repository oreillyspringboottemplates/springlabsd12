<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>Standard Form AJAX DTO/Entity</h1>

<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-12">
				<form method="post" id="form" action="<c:url value='standard-form-submit-dto' />">
					<div class="form-group col-sm-12">
						<div class="col-sm-6 form-group">
							<label for="firstName">First Name</label>
							<orly-input id="firstName" name="firstName" placeholder="First Name"></orly-input>
						</div>
						<div class="col-sm-6 form-group">
							<label for="lastName">Last Name</label>
							<orly-input id="lastName" name="lastName" placeholder="Last Name"></orly-input>
						</div>
						<div class="col-sm-3 form-group">
							<button id="submitBtn" type="submit" class="btn btn-primary">Submit</button>
							
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
// vanilla JS
document.getElementById("submitBtn").addEventListener("click", function(e){
	try {
		// Must STOP regular form submission when using a <form>
		// with an "action" attribute
		e.preventDefault(); 
		let data = {};
		data.firstName = document.getElementById("firstName").value;
		data.lastName = document.getElementById("lastName").value;
					
		// Make AJAX call to the server along with a message
		fetch("<c:url value='/forms/standard-form-ajax-dto' />", {
		        method: "POST",
		        body: JSON.stringify(data),
		        headers: {
		            "Content-Type": "application/json"
		        }
		}).then(function(response) {
			// Get the response, grab the JSON, and return response so we
			// can process it in the next "then" (since this is a Promise...)
		  	if (response.ok) {
		  		console.log("response.status=", response.status);
		  		let jsonReceived = response.json();		  		
		  		return jsonReceived;
		  	}
		}).then(function(response) {
			// do something with your JSON
			let jsonObject = JSON.parse(response);
			console.log("our JSON from the conroller = " , jsonObject);

		}).catch(function(error) {
			console.log('There was a problem with your fetch operation');
		});

	} catch (err) {
		// Do not show try-catch errors to the user in production
		console.log('Error: ' + err)
	}
});
</script>