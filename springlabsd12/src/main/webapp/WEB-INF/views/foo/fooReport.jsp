<%-- <%@page import="java.util.Date"%> --%>
<%@page import="java.util.Date"%>
<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1 class="center">Kung Foo LLC</h1>
<h2 class="center">Class Report</h2>
<div class="mb-2">
	<span>
		<b>Total Transactions</b>: ${txCount}
	</span>
	<span class="float-right">
		<%=new Date() %>
	</span>
</div>

<c:choose>
	<c:when test=""></c:when>
	<c:when test=""></c:when>
	<c:when test=""></c:when>
	<c:when test=""></c:when>
	<c:otherwise></c:otherwise>
</c:choose>

<c:forEach var="entry" items="${classMap}">
	<%-- Group By Value (Class Name) --%>
	<div class="oreillyGreen strong">
		<h3>${entry.key}</h3>
	</div>
	
	<%-- Course Information --%>
	<div class="ml-3">
		<c:set var="clazz" value="${entry.value}" />
		Instructor=${clazz.instructor} Days=${clazz.days} Start=${clazz.start} Duration=${clazz.duration}
	</div>

	<%-- Pupil Report --%>	
	<div class="ml-3">
		<table class="table table-striped table-bordered table-hover ml-3">
			<thead>
				<tr>
					<td>Transaction Id</td>
					<td>Class Id</td>
					<td>First Name</td>
					<td>Last Name</td>
					<%-- <td>${pupil.enrollDate}</td> --%>
				</tr>				
			</thead>
			<tbody>
				<c:forEach items="${clazz.pupilList}" var="pupil">
					<tr>
						<td>${pupil.txId}</td>
						<td>${pupil.classId}</td>
						<td>${pupil.firstName}</td>
						<td>${pupil.lastName}</td>
						<%-- <td>${pupil.enrollDate}</td> --%>
					</tr>
				</c:forEach>	
			</tbody>
		</table>

	</div>
	<hr/>
</c:forEach>

<c:forEach items="${classList}" var="clazz">
	${clazz}<br/>
</c:forEach>








