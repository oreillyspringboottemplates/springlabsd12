<%@ include file="/WEB-INF/layouts/include.jsp"%>
<nav class="nav nav-pills flex-column mt-2">
	<a class="${active eq 'add' ? 'nav-item nav-link active' : 'nav-item nav-link'}" 
	   href="<c:url value='/carparts/partnumber' />">Add Part Number</a>
	<a class="${active eq 'H4666ST' ? 'nav-item nav-link active' : 'nav-item nav-link'}"
		href="<c:url value='/carparts/electrical/H4666ST'/>">Headlight	Bulb</a> 
	<a class="${active eq 'M134HV' ? 'nav-item nav-link active' : 'nav-item nav-link'}"
		href="<c:url value='/carparts/engine/M134HV' />">Oil Pumps</a>
	<a class="${active eq '40026' ? 'nav-item nav-link active' : 'nav-item nav-link'}"
		href="<c:url value='/carparts/other/40026' />">Air Pump</a>
	<a class="${active eq 'contactus' ? 'nav-item nav-link active' : 'nav-item nav-link'}" 
	  	   href="<c:url value='/carparts/contactus' />">
		Contact Us
	</a>
	<a class="${active eq 'emailus' ? 'nav-item nav-link active' : 'nav-item nav-link'}" 
	  	   href="<c:url value='/carparts/emailus' />">
		Email Us
	</a>
	<a class="${active eq 'regular' ? 'nav-item nav-link active' : 'nav-item nav-link'}" 
	  	   href="<c:url value='/carparts/regular' />">
		Feedback
	</a>
</nav>