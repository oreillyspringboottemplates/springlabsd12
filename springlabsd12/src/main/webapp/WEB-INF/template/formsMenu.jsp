<%@ include file="/WEB-INF/layouts/include.jsp"%>

<nav class="navbar bg-light">
	<ul class="nav nav-pills flex-column">
		<li>
		    <a class="nav-link" 
		       href="<c:url value='/forms/query-string' />">
		    	Query String
		    </a>
		</li>
		<li>
		    <a class="nav-link" 
		       href="<c:url value='/forms/path-variables/Jane/Lane' />">
		    	Path Variables
		    </a>
		</li>
		<li>
		    <a class="nav-link" 
		       href="<c:url value='/forms/standard-form-submit-dto' />">
		    	Standard Form Submit with DTO/Entity
		    </a>
		</li>
		<li>
		    <a class="nav-link" 
		       href="<c:url value='/forms/standard-form-ajax-dto' />">
		    	Standard Form AJAX with DTO/Entity
		    </a>
		</li>
		<li>
		    <a class="nav-link" 
		       href="<c:url value='/forms/regular' />">
		    	Spring Form: Submit with DTO/Entity (previous feedback form example)
		    </a>
		</li>
		<li>
		    <a class="nav-link" 
		       href="<c:url value='/forms/standard-form-submit-variables' />">
		    	Standard Form Submit with Variables
		    </a>
		</li>
	</ul>
</nav>