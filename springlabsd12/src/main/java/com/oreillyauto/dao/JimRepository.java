package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.JimRepositoryCustom;
import com.oreillyauto.domain.Course;

/*
 *  CrudRepository<Example, Integer>  <== IMPORTANT: The integer is the datatype of your GUID (PK) on your table  
 */ 
public interface JimRepository extends CrudRepository<Course, Integer>, JimRepositoryCustom {
    
    // Add your interface abstract methods here (that ARE Spring Data methods)
    
}
