package com.oreillyauto.dao.impl;

import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.AddressBookRepositoryCustom;
import com.oreillyauto.domain.Address;
import com.oreillyauto.domain.QAddress;

@Repository
public class AddressBookRepositoryImpl extends QuerydslRepositorySupport implements AddressBookRepositoryCustom{
	
	QAddress addressTable = QAddress.address;
	
	public AddressBookRepositoryImpl() {
		super(Address.class);
	}
	@Override
	public List<Address> getAddresses() {
		return from(addressTable)
				.fetch();

	}
	@Override
	public Address getAddressById(Integer id) {
		return from(addressTable)
				.where(addressTable.id.eq(id))
				.limit(1)
				.fetchOne();
	}

}
