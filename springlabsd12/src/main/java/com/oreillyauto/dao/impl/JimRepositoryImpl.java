package com.oreillyauto.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.JimRepositoryCustom;
import com.oreillyauto.domain.Course;
import com.oreillyauto.domain.QCourse;
import com.oreillyauto.domain.QUniversity;
import com.oreillyauto.domain.University;
import com.querydsl.core.group.GroupBy;

@Repository
public class JimRepositoryImpl extends QuerydslRepositorySupport implements JimRepositoryCustom {
	// Add your queries here. Typed or QueryDSL...
	
	private QCourse coursesTable = QCourse.course;
	private QUniversity universityTable = QUniversity.university;

    public JimRepositoryImpl() {
        super(Course.class);
    }

    @Override
    public Map<String, Course> getDetails() {
        // TODO Auto-generated method stub
        Map<String, Course> map = from(coursesTable)
                .orderBy(coursesTable.courseName.asc())
                .transform(GroupBy.groupBy(coursesTable.courseName) // Map Key
                           .as(coursesTable)                        // Map Value
                          );
        
        for (Entry<String, Course> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue() );
        }
     
        return null;
    }

    public Map<String, List<University>> getReport() {
        Map<String, List<University>> map = from(coursesTable)
            .innerJoin(universityTable)
            .on(coursesTable.courseGuid.eq(universityTable.course.courseGuid))
            .orderBy(coursesTable.courseName.asc(), universityTable.universityName.asc())
            .transform(GroupBy.groupBy(coursesTable.courseName)  // Map Key
                .as(GroupBy.list(universityTable)));             // Map Value
        
        for (Entry<String, List<University>> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " : ");
            List<University> list = entry.getValue();
            for (University university : list) {
                System.out.println("\t" + university);
            }
        }
        
        return null;
    }

    
}

