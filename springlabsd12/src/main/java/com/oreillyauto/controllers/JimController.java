package com.oreillyauto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.oreillyauto.service.JimService;

@Controller
public class JimController {

	@Autowired
	JimService jimService;

    @GetMapping(value = { "/jim" })
    public String getJim(Model model) {
        jimService.getDetails();
        jimService.getReport();
        return "jim";
    }
    
 
}
