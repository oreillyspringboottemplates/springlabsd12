package com.oreillyauto.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.foo.Clazz;
import com.oreillyauto.domain.foo.Pupil;
import com.oreillyauto.model.Oreilly;
import com.oreillyauto.model.Response;
import com.oreillyauto.service.FooService;
import com.oreillyauto.util.Helper;

@Controller
public class FooController extends BaseController {
	
	@Autowired
	FooService fooService;
	
	@GetMapping(value = "/foo")
	public String getFoo(Model model) throws Exception {
		model.addAttribute("clazzList", fooService.getClasses());
		
		List<Pupil> pupilList = fooService.getPupils();
		model.addAttribute("pupilListJson", Helper.getJson(pupilList));
		
		return "foo";
	}
	
	@GetMapping(value = "/foo/report")
	public String getFooReport(Model model) throws Exception {
		Map<String, Clazz> clazzMap = fooService.getClassesQdsl();
		Integer txCount = fooService.getTransactionCount(clazzMap);
		model.addAttribute("txCount", txCount);
		model.addAttribute("classMap", clazzMap);
		return "fooReport";
	}
	
	@ResponseBody
    @PostMapping(value = { "/foo/addPupil" })
    public Response postDeleteUser(@RequestBody Pupil pupil) {
		Response response = new Response();
		
		try {
	    	if (pupil != null) {
	    		fooService.addPupil(pupil);
	    		response.setMessage("Student Successfully Enrolled");
		    	response.setMessageType(Oreilly.SUCCESS);
		    	response.setPupilList(fooService.getPupils());
	    	} else {
	    		response.setMessage("Sorry, unable to add the student.");
		    	response.setMessageType(Oreilly.DANGER);	
	    	}
	    	
	    	return response;
		} catch (Exception e) {
			e.printStackTrace();
			response.setMessage(Helper.getError(e));
	    	response.setMessageType(Oreilly.DANGER);
	    	return response;
		}
    }
}
