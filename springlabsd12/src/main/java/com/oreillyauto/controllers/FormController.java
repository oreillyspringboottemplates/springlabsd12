package com.oreillyauto.controllers;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.dto.Email;
import com.oreillyauto.model.Form;

@Controller
public class FormController extends BaseController {

    @GetMapping(value = "/forms")
    public String getFormHome() {
        return "formsHome";
    }
    
    /* Query String variables
     * with default values  */
    @GetMapping(value = "/forms/query-string")
    public String getQueryString(@RequestParam(defaultValue = "missing fn") String fn,
                                 @RequestParam(defaultValue = "missing ln") String ln) {
        System.out.println("fn:" + fn + " ln:" + ln);
        return "query-string";
    }
    
    
    /* use different variable names than what comes in */
/*	@GetMapping(value = "/forms/query-string")
	public String getQueryString(@RequestParam(name="fn") String firstName, String ln) {
	    System.out.println("firstName: " + firstName + " ln: " + ln);
		return "query-string";
	}*/
	
	
    /* Multiple path variables */
    @GetMapping(value = {"/forms/path-variables", "/forms/path-variables/{fn}/{ln}"})
    public String getPathVariables(@PathVariable String fn, @PathVariable String ln) {
        System.out.println("fn: " + fn + " ln: " + ln);
        return "path-variables";
    }
    
    @GetMapping(value = "/forms/standard-form-submit-dto")
    public String getStandardFormSubmitDTO() {
        return "standard-form-submit-dto";
    }
    
    @PostMapping(value = "/forms/standard-form-submit-dto")
    public String postStandardFormSubmitDTO(Form form) {
        System.out.println(form);
        return "standard-form-submit-dto";
    }
    
    @GetMapping(value = "/forms/standard-form-ajax-dto")
    public String getStandardFormAJAXDTO() {
        return "standard-form-ajax-dto";
    }
    
    @ResponseBody
    @PostMapping(value = "/forms/standard-form-ajax-dto")
    public String postStandardFormAJAXDTO(@RequestBody Form form) {
    	try {
	        System.out.println(form);
	        return "{\"message\":\"Simple JSON object returned\"}";
    	} catch(Exception e) {
    		return "{\"message\":\"ERROR! RUN FOR YOUR LOVES, BEFFERY!\"}";
    	}
    }
    
    
    @GetMapping(value = "/forms/standard-form-submit-variables")
    public String getStandardFormWithVariables() {
        return "standard-form-submit-variables";
    }
    
/*    @PostMapping(value = "/forms/standard-form-submit-variables")
    public String postStandardFormWithVariables(@RequestParam String firstName, @RequestParam String lastName) {
        System.out.println("First Name: " + firstName + " Last Name: " + lastName);
        return "standard-form-submit-variables";
    }*/
    
    @PostMapping(value = "/forms/standard-form-submit-variables")
    public String postStandardFormWithVariables(@RequestParam Map<String,String> allParams) {
        System.out.println("Map: " + allParams);
        return "standard-form-submit-variables";
    }
    
	// Feedback "Regular" Spring MVC GET Request 
	@GetMapping(value = "/forms/regular")
	public String getRegularForm(Model model) {
	    Email email = new Email();
	    model.addAttribute("command", email);
	    model.addAttribute("active", "regular");
	    return "springForm";
	}
	
    // Feedback "Regular" Spring MVC POST Request
    @PostMapping(value = "/forms/feedback")
    public String postRegularForm(@ModelAttribute("command") Email email, BindingResult result, 
    		Model model) {
    	
        System.out.println("from=>" + email.getEmailAddress() + " body=>" + email.getEmailBody());
        model.addAttribute("messageType", "success");
        model.addAttribute("message", "Success! Thank you for your feedback!");
        model.addAttribute("command", new Email());
        
        // Redirect
        return "redirect:/forms/regular";
    }
}
