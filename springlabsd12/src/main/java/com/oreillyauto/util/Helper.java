package com.oreillyauto.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.oreillyauto.domain.Example;

public class Helper {

	public static boolean isInteger(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static String getJson(List<?> list) throws JsonGenerationException, JsonMappingException, IOException {
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		final ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(out, list);
		final byte[] data = out.toByteArray();
		return new String(data);
		//return new Gson().toJson(list);
	}

	public static String getJson(Object obj) throws JsonProcessingException {
		if (obj == null) {
			return new JsonObject().toString();
		}

		return new ObjectMapper().writeValueAsString(obj);
	}

	public static boolean hasItems(List<?> list) {
		if (list != null && list.size() > 0) {
			return true;
		}

		return false;
	}
	
	public static void printExampleList(List<Example> exampleList) {
		if (exampleList != null && exampleList.size() > 0) {
			for (Example example: exampleList) {
				System.out.println((Example)example);
			}
		}
	}
	
	public static void printList(List<?> list) {
		if (list != null && list.size() > 0) {
			for (Object object : list) {
				System.out.println(object.toString());
			}
		}
	}

	public static String getError(Exception e) {
		return ((e == null) ? "Null Pointer Exception" : e.getMessage());
	}

	public static void setMessageOnSession(HttpSession session, String messageType, String message) {
		session.setAttribute("message", message);
		session.setAttribute("messageType", messageType);
	}

	public static String getErrorMessage(Exception e) {
		return (e == null || e.getMessage() == null) ? 
				"Null Pointer Exception" : e.getMessage();
	}
	
}
