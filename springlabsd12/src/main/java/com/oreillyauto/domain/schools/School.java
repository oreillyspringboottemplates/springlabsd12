package com.oreillyauto.domain.schools;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="SCHOOLS")
public class School implements Serializable {
	private static final long serialVersionUID = -5996834027120348470L;
    public School() {}
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "school_id", columnDefinition = "INTEGER")
    private Integer schoolId;

    @Column(name = "school_name", columnDefinition = "VARCHAR(64)")
	private String schoolName;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "school_id")
    private List<Student> studentList = new ArrayList<Student>();
    
	public Integer getSchoolId() {
		return schoolId;
	}
	
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public List<Student> getStudentList() {
		return studentList;
	}

	public void setStudentList(List<Student> studentList) {
		this.studentList = studentList;
	}

	@Override
	public String toString() {
		return "School [schoolId=" + schoolId + ", schoolName=" + schoolName + "]";
	}
    
}
