package com.oreillyauto.domain.orders;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="ORDER_ITEMS")
public class OrderItem implements Serializable {
	private static final long serialVersionUID = -5996834027120348470L;
    public OrderItem() {}
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "item_id", columnDefinition = "INTEGER")
    private Integer itemId;
    
    @Column(name = "sku", columnDefinition = "INTEGER")
    private Integer sku;
    
    // Unidirectional Mapping
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="order_id")
    private Order order;

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public Integer getSku() {
		return sku;
	}

	public void setSku(Integer sku) {
		this.sku = sku;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "OrderItem [itemId=" + itemId + ", sku=" + sku + ", order=" + order + "]";
	}
    
}
