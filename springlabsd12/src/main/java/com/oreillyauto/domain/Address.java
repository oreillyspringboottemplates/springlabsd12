package com.oreillyauto.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="address_book")
public class Address implements Serializable{

	/**
	 * Beff Hates This
	 */
	private static final long serialVersionUID = 2423872248669045966L;

	@Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "INTEGER")
    private Integer id;
	
	@Column(name="first_name", columnDefinition = "VARCHAR(256)")
	private String firstName;
	
	@Column(name="last_name", columnDefinition = "VARCHAR(256)")
	private String lastName;
	
	@Column(name="address_one", columnDefinition = "VARCHAR(256)")
	private String addressOne;
	
	@Column(name="address_two", columnDefinition = "VARCHAR(256)")
	private String addressTwo;
	
	@Column(name="city", columnDefinition = "VARCHAR(256)")
	private String city;
	
	@Column(name="state", columnDefinition = "VARCHAR(256)")
	private String state;
	
	@Column(name="zip", columnDefinition = "VARCHAR(256)")
	private String zip;
	
	@Transient
	private String message;
	
	@Transient
	private String messageType;


	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddressOne() {
		return addressOne;
	}

	public void setAddressOne(String addressOne) {
		this.addressOne = addressOne;
	}

	public String getAddressTwo() {
		return addressTwo;
	}

	public void setAddressTwo(String addressTwo) {
		this.addressTwo = addressTwo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", addressOne="
				+ addressOne + ", addressTwo=" + addressTwo + ", city=" + city + ", state=" + state + ", zip=" + zip
				+ "]";
	}

	public Address() {
		super();
	}
}
